Splunk.Module.BulletChart = $.klass(Splunk.Module, {
    initialize: function ($super, container) {
        

        $super(container);

        //this.div_id = '#' + this.moduleId + '_bulletChart';
        this.div_id = '#_bulletChart';
        this.svg_id = this.moduleId + '_svg';

        $script(Splunk.util.make_url('/modules/BulletChart/BulletChart.v1.js'), 'bullet_js');

    },
    
    displayChart: function(data){
        var div_id = this.div_id;
        data = d3.csv.parse(data);
        var max_overall=0.0;
        data.forEach(function(o) {
            o.title=o.name;
            o.subtitle=o.value+o.unit;
            o.avg = parseFloat(o.avg);
            o.max=parseFloat(o.max);
            o.value = parseFloat(o.value);
            o.threshold = parseFloat(o.threshold);
            if(o.max > max_overall){
                max_overall=o.max;
            }
        });
        data.forEach(function(o) {
            o.ranges=[o.avg,o.max,max_overall];
            o.measures=[o.value];
            o.markers=[o.threshold];
            delete o.name; delete o.unit; delete o.avg; delete o.max;
            delete o.value; delete o.threshold;
        });
        
        var margin = {top: 5, right: 40, bottom: 20, left: 120},
        width = 400 - margin.left - margin.right,
        height = 50 - margin.top - margin.bottom;
    
        var chart = d3.bullet()
            .width(width)
            .height(height);
            
        var temp1 = d3.select(div_id);
        var temp2 = temp1.selectAll("svg");
        var temp3 = temp2.data(data).enter().append("svg");
        var temp4 = temp3.attr("class","bullet");
        var temp5 = temp4.attr("width", width + margin.left + margin.right);
        var temp6 = temp5.attr("height", height + margin.top + margin.bottom);
        var temp7 = temp6.append("g");
        var temp8 = temp7.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var svg_bulletChart = temp8.call(chart);
        
        
        //var svg_bulletChart = d3.select(div_id).selectAll("svg")
        //    .data(data)
        //    .enter().append("svg")
        //    .attr("class","bullet")
        //    .attr("width", width + margin.left + margin.right)
        //    .attr("height", height + margin.top + margin.bottom)
        //    .append("g")
        //    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        //    .call(chart);
            
        var title = svg_bulletChart.append("g")
            .style("text-anchor", "end")
            .attr("transform", "translate(-6," + height / 2 + ")");
      
        title.append("text")
            .attr("class", "title")
            .text(function(d) { return d.title; });
      
        title.append("text")
            .attr("class", "subtitle")
            .attr("dy", "1em")
            .text(function(d) { return d.subtitle; });
        
        //remove ticks except those in the last row
        var ticks=d3.selectAll("g .tick")[0];
        ticks = ticks.slice(0,(-1)*ticks.length/title[0].length);
        d3.selectAll(ticks).remove();
      
        d3.selectAll("button").on("click", function() {
          svg_bulletChart.datum(randomize).call(chart.duration(1000)); // TODO automatic transition
        });
        
        function randomize(d) {
          if (!d.randomizer) d.randomizer = randomizer(d);
          d.ranges = d.ranges.map(d.randomizer);
          d.markers = d.markers.map(d.randomizer);
          d.measures = d.measures.map(d.randomizer);
          return d;
        }
        
        function randomizer(d) {
          var k = d3.max(d.ranges) * .2;
          return function(d) {
            return Math.max(0, d + k * (Math.random() - .5));
          };
        }
        
    },

  
    getModifiedContext: function() {
    
        //alert("get modified cube.");
        
        var that = this,
            context = this.getContext(),
            //namespace = context.get('namespace'),
            //nsobj = context.get(namespace),
            dataset = context.get('dataset');
            //name = this.varname; 

        //alert("get modified cube : " + dataset);

        if (dataset != undefined) {
            this.displayChart(dataset.results);
        }
    },

    onContextChange: function () { 
    
         //alert("on context cube.");
        
         var that = this,
            context = this.getContext(),
            //namespace = context.get('namespace'),
            //nsobj = context.get(namespace),
            dataset = context.get('dataset');
            //name = this.varname; 
     
        //alert("on context : " + dataset); 

        if (dataset != undefined) {
            this.displayChart(dataset.results);
        }
    }

});